var exec = require("child_process").exec;
let fs = require('fs');

function iniciar(resp) {
  console.log("Manipulador de petición 'iniciar' fue llamado.");


    //var body = require('colando.html');

    fs.readFile("colando.html", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/html' });
                resp.write(pgResp);
            }
             
            resp.end();
        });

  
    //response.writeHead(200, {"Content-Type": "text/html"});
    //response.write(body);
    //response.end();
}

function subir(response) {
  console.log("Manipulador de petición 'subir' fue llamado.");
  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("<b>subiendo archivo a node js </b>");
  response.end();
}

function colar(response){
  console.log("<h1>colando un rico cafe.</h1>");
  var smart_cafe = require('./smart_cafe');

  smart_cafe.colarPorPeticion();

  response.writeHead(200, {"Content-Type": "text/html"});
  response.write("<b>colando un rico cafe</b>");
  response.end();
}


exports.iniciar = iniciar;
exports.subir = subir;
exports.colar = colar;