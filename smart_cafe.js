var five = require("johnny-five");
var board = new five.Board();
var colando_cafe = false; 
var arduino_connect = false;
var led_colar = null;
var led_titilar = null;

board.on("ready", function() {
  // Create an Led on pin 12
  var led = new five.Led(12);
  var led2 = new five.Led(8);
  var led3 = new five.Led(7);
  
  // Blink every half second
  var led4 = five.Led(2);
  led4.blink(100);
  console.log("encendiendo mi cafetera inteligente");
  led_titilar = led4;
  led_colar = led;

  this.repl.inject({
  	led1: led
  });

  this.repl.inject({
  	led2: led2
  });

  this.repl.inject({
  	led3: led3
  });

  setInterval(function(){
  	
  	var resp = verificarFecha(led);

  }, 3000);

  arduino_connect = true;
});

board.on("error", function(err){
	console.log("Esto salio mal: "+err)
});


function verificarFecha(led){

	var datetime = new Date();

	var hora = datetime.getHours()+':'+datetime.getMinutes();
		
	if(hora == '18:20'){
		if(colando_cafe== false){
			led.on();	
			colando_cafe = true;
		
			//apagar luego de 30 seg
			setTimeout(function(){ apagarCafetera(led)}, 60000);	
			console.log("hora del cafe... " );

		}
		
		//encender
		return true;
	}else{
		return false;	
	}

	
}

function colarPorPeticion(){
	console.log("colando_cafe" + colando_cafe);
	if(colando_cafe== false){
		
		if(arduino_connect == false)
		{
			board.on("ready", function() {
				console.log('preparando cafetera')
				colando_cafe = true;
				var led = new five.Led(12);
				led.on();

				//apagar luego de 30 seg
				setTimeout(function(){ apagarCafetera(led_colar, led_titilar)}, 60000);	
				console.log("hora del cafe... " );
				arduino_connect = true;
			});	
		}else{
			console.log('preparando cafetera')
			colando_cafe = true;
			var led = new five.Led(12);
			led.on();
			
			//apagar luego de 30 seg
			setTimeout(function(){ apagarCafetera(led_colar, led_titilar)}, 60000);	
			console.log("hora del cafe... " );
		}		
		

	}else{
		console.log("aguarde por favor... ya va a tener cafe");
	}
}

function apagarCafetera(led, titilador){
	console.log("ya esta listo su cafe... apagando...");
	colando_cafe = false;  
	led.off();
	titilador.stop();
	//setTimeout(function(){ process.exit();}, 10000);
	
}

exports.colarPorPeticion = colarPorPeticion;