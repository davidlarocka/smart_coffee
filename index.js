var server  = require('./server');
var router = require("./router");
var acciones = require('./manipulador');

var handle = {}
handle["/"] = acciones.iniciar;
handle["/subir"] = acciones.subir;
handle["/colar"] = acciones.colar;

server.iniciarServer(router.route, handle);